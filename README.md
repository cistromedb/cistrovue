# cistrovue

This is the client-side of Cistrome Data Browser based on vue.js version 2. Here will describe how to build it on your local machine and publish to the production environment.

## Project local setup

First, you need to install node.js environment on your machine. We recommend you install node.js via [nvm](https://github.com/nvm-sh/nvm#install--update-script). nvm is a version control package for node.js, which is similar to use conda to switch python2 or python3.

Then you can install node.js by following command:

```bash
nvm install node # "node" is an alias for the latest version
```

After that, type the following command to install the needed packages for cistrovue. This command will automatically install the packages defined in `package.json`. Also, we appreciate that you prompt add necessary packages or remove unneeded packages in `package.json` for easier maintaining our project in the future.

```bash
npm install
```

After installation of those packages, you can compile and hot-reloads the project for development by the following command:

```bash
npm run serve
```

When the command finished the execution, the browser will open a new tab that can refresh your change automatically.

## Project development

This part describe that how to develop this project which is mainly introduce the brief idea of this whole project.

In vue.js, most configuration was defined in `vue.config.js`. You can find the detail instructure [here](https://cli.vuejs.org/config/).

The codes you need to modify are located in `src` folder. In `src` folder, all codes were starts with `main.ts`. In `main.ts`, we import the packages that this project used. `vue-router` was used for webpage switching ([document](https://router.vuejs.org/)), `BootstrapVue` for style of the webpages ([document](https://bootstrap-vue.org/docs)), `axios` for ajax request ([document](https://github.com/axios/axios)).

The `App.vue` is the header of every webpage, and the tag `router-view` is the position where different webpages' content was changed. The router configuration was defined in `src/router/index.ts`. The content of each page was coded in `src/views`. Some componments that can be reused was coded in `src/components`. The relationship of each component can be found at each `views` page. Also, the css for each component or page can be found in in `src/styles`.

Others, the `src/api/bus.ts` is a hack that used to pass the value for different component.

### Compiles and minifies for production

```bash
npm run build
```

### Run your unit tests

```bash
npm run test:unit
```

### Lints and fixes files

```bash
npm run lint
```

