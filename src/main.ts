import Vue from 'vue'
Vue.config.productionTip = false
// main page
import App from './App.vue'
// router for switching pages
import router from './router'
// bootstrap css for style
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

// ajax library for query api
import axios from 'axios'
import Vueaxios from 'vue-axios'
Vue.use(Vueaxios, axios)
// setting for axios
axios.defaults.timeout = 60*1000 // Timeout
axios.defaults.baseURL = 'http://dc2.cistrome.org/api/'
// axios.defaults.baseURL = 'http://127.0.0.1:8000/'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

