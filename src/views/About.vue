<template>
  <div class="container">
    <div class="row pl-3 pr-3 ml-3 mr-3">
      <div class="col-12">
        <h2>Supplementary Material</h2>
        <h4>Tools and parameters used for data analysis</h4>
        <p>
          Cistrome DB uses the standard analysis pipeline ChiLin (1) to process all chromatin profiling reads.
          Here is a brief introduction to the tools and parameters used for data analysis.
        </p>
        <p>
          <b>read mapping</b>: Mapping is done using bwa (2).
        </p>
        <p>
          <code>bwa aln -q 5 -l 32 -k 2 -t 8 index FASTQ > sai</code>
        </p>
        <p>
          <code>bwa samse index sai FASTQ > sam</code>
        </p>
        <p>
          <b>read filtering</b>: Processing of mapped reads is carried out using samtools (3).
        </p>
        <p>
          <code>samtools view -bS -t chromInfo_file.txt -q 1 sam > bam</code>
        </p>
        <p>
          <b>read sorting</b>:
        </p>
        <p>
          <code>samtools sort -m 4000000000 bam > bam_sort</code>
        </p>
        <p>
          <b>read mapping statistics</b>:
        </p>
        <p>
          <code>samtools flagstat bam > bam.stat</code>
        </p>
        <p>
          <b>peak calling</b>: Peaks are identified using MACS2 (4).
        </p>
        <p>
          <code>macs2 callpeak --SPMR -B -q 0.01 --keep-dup 1 --extsize=146 --nomodel -g hs -t bam -n test</code>
        </p>
        <p>
          <b>bigwiggle generation</b>: Signal tracks for browser viewer are generated using BEDTools (5) and UCSC tools (6).
        </p>
        <p>
          <code>bedtools intersect -a bedGraph -b chrom.bed -wa -f 1.00 > bedGraph.tmp</code>
        </p>
        <p>
          <code>bedGraphToBigWig bedGraph.tmp chromInfo_file.txt bigwiggle</code>
        </p>
        <p>
          <b>motif scanning</b>: Motif significance is based on an analysis of the position of motifs
          relative to peak summits (7). In samples with more than 5,000 peaks this analysis if
          based on the most significant 5,000 peak.
        </p>
        <p>
          <code>MDSeqPos.py -d -w 600 -p 0.001 -m cistrome.xml -O output bed hg38</code>
        </p>
        <p>
          <b>target gene analysis</b>: Target genes are identified using BETA (8).
          This function is also available at
          <a
            href="https://github.com/cfce/chilin/tree/master/chilin2/modules/regulatory"
          >here</a> .
          Peaks with less than 5 fold signal to background ratio are filtered out,
          and the most significant 10,000 peaks amongst these are selected.
          A regulatory potential for is calculated for each RefSeq gene:
        </p>
        <p>
          <code>RegPotential.py -t top -g refGene -n test -d 100000</code>
        </p>
        <p>
          In the development of CistromeDB we found that in many cases the metadata provided
          in GEO did not provide a clear way to match samples with controls. Furthermore,
          we discovered that controls were of highly variable quality and it was not clear that
          inclusion of controls produced more accurate results in all cases. We therefore decided
          to exclude control samples in the peak identification. Nevertheless, in the peak
          identification step MACS2 estimates background signal and in the target prediction
          step a 5-fold ratio of signal to estimate background is used to filter out low quality peaks.
        </p>
        <h4>Quality control metrics definition and thresholds</h4>
        <p>
          Quality control (QC) metrics are generated through ChiLin(1) and are similar to the
          ENCODE consortium guidelines. Based on the collection of ~23,000 samples, we set thresholds
          to help the user to judge overall data quality. We draw the cumulative percentage plot
          of quality control metrics for all historic data and make thresholds to filter high quality
          samples (Figure S1). Thresholds of PBC and FRiP score are inherited from the ENCODE consortium (3)
          and other quality control metrics thresholds are based on our data collection and experience.
          Although thresholds serve as a convenient way for users to evaluate data quality based on
          QC metrics, they are somewhat arbitrary, and there can be large differences in the true binding
          strengths and genome-wide distributions of different factors.
        </p>
        <ul>
          <li>
            <b>Raw sequence median quality score</b> is the sample's median sequence quality. We calculate
            these scores using the FastQC software2 (9). A good sequence quality score is
            <span
              class="highlightPass"
            >>=25</span>.
          </li>
          <li>
            <b>The uniquely mapped read number</b> is the number of reads with bwa mapping quality above 1.
            The corresponding "uniquely mapped ratio" is the number of uniquely mapped reads divided by
            the total number of reads. A good uniquely mapped ratio is
            <span
              class="highlightPass"
            >>=60%</span>.
          </li>
          <li>
            <b>PBC</b> (10) is the number of locations with exactly one uniquely mapped read divided by the
            number of unique locations. A good PBC score is
            <span
              class="highlightPass"
            >>=80%</span>.
          </li>
          <li>
            <b>FRiP</b> (10) (Fraction of Reads in Peaks) is a quality measure for ChIP-seq and DNase-seq
            experiments. FRiP is the percentage of uniquely mapped tags from autosomal chromosomes that
            fall in MACS2 peaks. ChiLin calculates FRiP from a sub-sampling of 4M uniquely mapped reads.
            A good FRiP score is
            <span
              class="highlightPass"
            >>=1%</span>.
          </li>
          <li>
            <b>10 fold and 20 fold confident peaks</b> are the number of peaks called by MACS2 (4)
            where the fold change is greater than 10 or 20. A large number of 10 and 20 fold confident
            peaks indicates good quality of the ChIP samples.
          </li>
          <li>
            <b>The union DHS (DNase I hypersensitive site) overlap</b> is the proportion of the most significant
            5,000 peaks (ordered by MACS2 -log (qvalue)) that overlap with a union of DNase-seq peaks.
            ChiLin derives the union DHS by merging all the peaks of DNase-seq data from ENCODE (11),
            which represent a comprehensive set of regulatory elements across many cell lines and tissues
            in the human and mouse genomes. This is expected to be
            <span
              class="highlightPass"
            >>=70%</span>.
          </li>
          <li>
            <b>The exon, intron, intergenic and promoter ratios of peak summits</b> is based on the RefSeq
            gene annotation. ChiLin reports the ratio of peak summits that overlap with exon, intron,
            intergenic, and promoter regions.
          </li>
        </ul>

        <img src="../assets/aboutFigureS1.png" class="img-fluid mx-auto d-block" />
        <p
          class="text-center"
        >Figure S1. Cumulative distribution of quality control metrics and thresholds.</p>
        <h4>References</h4>
        <ol>
          <li>Qian Qin,Shenglin Mei,Qiu Wu,X. Shirley Liu. (2016) ChiLin: a comprehensive ChIP-seq and DNase-seq quality control and analysis pipeline. BMC bioinformatics.</li>
          <li>Li, H. and Durbin, R. (2009) Fast and accurate short read alignment with Burrows-Wheeler transform. Bioinformatics, 25, 1754-1760.</li>
          <li>Li, H., Handsaker, B., Wysoker, A., Fennell, T., Ruan, J., Homer, N., Marth, G., Abecasis, G., Durbin, R. and Genome Project Data Processing, S. (2009) The Sequence Alignment/Map format and SAMtools. Bioinformatics, 25, 2078-2079.</li>
          <li>Zhang, Y., Liu, T., Meyer, C.A., Eeckhoute, J., Johnson, D.S., Bernstein, B.E., Nusbaum, C., Myers, R.M., Brown, M., Li, W. et al. (2008) Model-based analysis of ChIP-Seq (MACS). Genome biology, 9, R137.</li>
          <li>Quinlan, A.R. and Hall, I.M. (2010) BEDTools: a flexible suite of utilities for comparing genomic features. Bioinformatics, 26, 841-842.</li>
          <li>Kent, W.J., Zweig, A.S., Barber, G., Hinrichs, A.S. and Karolchik, D. (2010) BigWig and BigBed: enabling browsing of large distributed datasets. Bioinformatics, 26, 2204-2207.</li>
          <li>Liu, T., Ortiz, J.A., Taing, L., Meyer, C.A., Lee, B., Zhang, Y., Shin, H., Wong, S.S., Ma, J., Lei, Y. et al. (2011) Cistrome: an integrative platform for transcriptional regulation studies. Genome biology, 12, R83.</li>
          <li>Wang, S., Sun, H., Ma, J., Zang, C., Wang, C., Wang, J., Tang, Q., Meyer, C.A., Zhang, Y. and Liu, X.S. (2013) Target analysis by integration of transcriptome and ChIP-seq data with BETA. Nature protocols, 8, 2502-2515.</li>
          <li>Andrews S: FastQC: A quality control tool for high throughput sequence data. 2010.</li>
          <li>Landt, S.G., Marinov, G.K., Kundaje, A., Kheradpour, P., Pauli, F., Batzoglou, S., Bernstein, B.E., Bickel, P., Brown, J.B., Cayting, P. et al. (2012) ChIP-seq guidelines and practices of the ENCODE and modENCODE consortia. Genome research, 22, 1813-1831.</li>
          <li>Zhang, Y., Liu, T., Meyer, C.A., Eeckhoute, J., Johnson, D.S., Bernstein, B.E., Nusbaum, C., Myers, R.M., Brown, M., Li, W. et al. (2008) Model-based analysis of ChIP-Seq (MACS). Genome biology, 9, R137.</li>
          <li>Thurman, R.E., Rynes, E., Humbert, R., Vierstra, J., Maurano, M.T., Haugen, E., Sheffield, N.C., Stergachis, A.B., Wang, H., Vernot, B. et al. (2012) The accessible chromatin landscape of the human genome. Nature, 489, 75-82.</li>
          <li>Mei, Shenglin, Qin, Qian, Wu, Qiu Sun, Hanfei Zheng, Rongbin Zang, Chongzhi Zhu, Muyuan Wu, Jiaxin Shi, Xiaohui Taing, Len Liu, Tao Brown, Myles Meyer, Clifford A. Liu, X. Shirley et al. (2016) Cistrome Data Browser: a data portal for ChIP-Seq and chromatin accessibility data in human and mouse Nucleic Acid Research</li>
        </ol>
      </div>
    </div>
  </div>
</template>


<style scoped lang="sass">
@import "../styles/document.scss"
</style>